package com.example.rxjava.observer;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.ArrayCompositeDisposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.HasUpstreamObservableSource;
import io.reactivex.observers.SerializedObserver;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.subjects.PublishSubject;

public class PausableObservable<T> extends Observable<T> implements HasUpstreamObservableSource<T> {

	public static final boolean RESUME = true;
	public static final boolean PAUSE = false;

	/** The source consumable Observable. */
	protected final ObservableSource<T> source;

	/**
	 * Constructs the ObservableSource with the given consumable.
	 * @param source the consumable Observable
	 */
	final ObservableSource<Boolean> other;
	private T lastItemOnPause;

	public PausableObservable(ObservableSource<T> source, ObservableSource<Boolean> other) {
		this.source = source;
		this.other = other;
		lastItemOnPause = null;
	}

	@Override
	public ObservableSource<T> source() {
		return source;
	}

	@Override
	public void subscribeActual(Observer<? super T> child) {

		final SerializedObserver<T> serial = new SerializedObserver<T>(child);

		final ArrayCompositeDisposable frc = new ArrayCompositeDisposable(2);

		serial.onSubscribe(frc);

		final PausableObserver<T> sus = new PausableObserver<T>(serial, frc) {
			@Override
			public void onNext(T t) {
				if (notSkipping == RESUME) {
					super.onNext(t);
					lastItemOnPause = null;
				} else if (notSkipping == PAUSE) {
					lastItemOnPause = t;
				}
			}
		};

		other.subscribe(new Observer<Boolean>() {
			Disposable s;

			@Override
			public void onSubscribe(Disposable s) {
				if (DisposableHelper.validate(this.s, s)) {
					this.s = s;
					frc.setResource(1, s);
				}
			}

			@Override
			public void onNext(Boolean t) {
				if (t == RESUME) {
					sus.notSkipping = RESUME;
				} else {
					sus.notSkipping = PAUSE;
				}

				emitLatestStashedItem(t);
			}

			private void emitLatestStashedItem(Boolean t) {
				if (t == PAUSE) {
					// throw
					return;
				}

				if (lastItemOnPause != null) {
					sus.onNext(lastItemOnPause);
				}
			}

			@Override
			public void onError(Throwable t) {
				s.dispose();
				frc.dispose();
				serial.onError(t);
			}

			@Override
			public void onComplete() {
				s.dispose();
				sus.notSkipping = true;
			}
		});

		source.subscribe(sus);
	}

	private class PausableObserver<OT> implements Observer<OT> {
		final Observer<? super OT> actual;
		final ArrayCompositeDisposable frc;

		Disposable s;

		volatile boolean notSkipping = RESUME;

		PausableObserver(Observer<? super OT> actual, ArrayCompositeDisposable frc) {
			this.actual = actual;
			this.frc = frc;
		}

		@Override
		public void onSubscribe(Disposable s) {
			if (DisposableHelper.validate(this.s, s)) {
				this.s = s;
				frc.setResource(0, s);
			}
		}

		@Override
		public void onNext(OT t) {
			actual.onNext(t);
		}

		@Override
		public void onError(Throwable t) {
			frc.dispose();
			actual.onError(t);
		}

		@Override
		public void onComplete() {
			frc.dispose();
			actual.onComplete();
		}
	}

	public static  <T> Observable<T> pausable(PublishSubject<T> subject, ObservableSource<Boolean> other) {
		ObjectHelper.requireNonNull(other, "other is null");
		return RxJavaPlugins.onAssembly(new PausableObservable<>(subject, other));
	}
}
package com.example.rxjava.observer;

import io.reactivex.observers.TestObserver;
import io.reactivex.subjects.PublishSubject;
import org.junit.Before;
import org.junit.Test;

/**
 * pause, resume 상태에 따라 event를 처리하는 Observable
 * pause 상태에서는 event가 전달되지 않으며, resume 상태에서만 event가 전달된다
 * pause 시점에 전파된 event가 있다면 resume 상태로 전환 시 가장 최근의 event를 전달한다
 * io.reactivex.internal.operators.observable.ObservableTakeUntil 을 응용
 */
public class PausableObservableTest {
	private PublishSubject<Boolean> pauseableSubject;
	private TestObserver<Integer> testObserver;
	private PublishSubject<Integer> sourceSubject = PublishSubject.create();

	@Before
	public void setUp() throws Exception {
		pauseableSubject = PublishSubject.create();
		testObserver = new TestObserver<>();

		PausableObservable.pausable(sourceSubject, pauseableSubject)
			.subscribe(testObserver);
	}

	@Test
	public void nothing() throws Exception {

		sourceSubject.onNext(1);
		sourceSubject.onNext(2);
		sourceSubject.onNext(3);

		testObserver.assertValues(1, 2, 3);
	}

	@Test
	public void pause() throws Exception {

		sourceSubject.onNext(1);
		pauseableSubject.onNext(PausableObservable.PAUSE);
		sourceSubject.onNext(2);
		sourceSubject.onNext(3);

		testObserver.assertValues(1);
	}

	@Test
	public void startFromPause() throws Exception {
		pauseableSubject.onNext(PausableObservable.PAUSE);
		sourceSubject.onNext(1);
		sourceSubject.onNext(2);
		sourceSubject.onNext(3);

		testObserver.assertNoValues();
	}

	@Test
	public void resume() throws Exception {
		sourceSubject.onNext(1);
		pauseableSubject.onNext(PausableObservable.RESUME);
		sourceSubject.onNext(2);
		sourceSubject.onNext(3);

		testObserver.assertValues(1, 2, 3);
	}

	@Test
	public void startFromResume() throws Exception {
		pauseableSubject.onNext(PausableObservable.RESUME);
		sourceSubject.onNext(1);
		sourceSubject.onNext(2);
		sourceSubject.onNext(3);

		testObserver.assertValues(1, 2, 3);
	}

	@Test
	public void resume_immediately_after_pause() throws Exception {
		sourceSubject.onNext(1);
		pauseableSubject.onNext(PausableObservable.PAUSE);
		pauseableSubject.onNext(PausableObservable.RESUME);
		sourceSubject.onNext(2);
		sourceSubject.onNext(3);

		testObserver.assertValues(1, 2, 3);
	}

	@Test
	public void endWIthResume() throws Exception {
		sourceSubject.onNext(1);
		sourceSubject.onNext(2);
		pauseableSubject.onNext(PausableObservable.PAUSE);
		sourceSubject.onNext(3);
		pauseableSubject.onNext(PausableObservable.RESUME);

		testObserver.assertValues(1, 2, 3);
	}

	@Test
	public void endWithResume_emitMoreThanOneItemsInPauseState() throws Exception {
		sourceSubject.onNext(1);
		sourceSubject.onNext(2);
		pauseableSubject.onNext(PausableObservable.PAUSE);
		sourceSubject.onNext(3);
		sourceSubject.onNext(4);
		sourceSubject.onNext(5);
		pauseableSubject.onNext(PausableObservable.RESUME);
		sourceSubject.onNext(6);
		sourceSubject.onNext(7);

		testObserver.assertValues(1, 2, 5, 6, 7);
	}
}